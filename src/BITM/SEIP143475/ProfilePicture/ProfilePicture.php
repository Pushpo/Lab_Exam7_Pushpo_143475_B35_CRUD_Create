<?php

namespace App\ProfilePicture;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

use PDO;

class ProfilePicture extends DB
{
    public $id = "";
    public $name = "user";
    public $profile_picture = "";

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        //echo "index profile_picture";
    }

    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }


    }
    public function setImageData($data=NULL)
    {
        if (array_key_exists('profile_picture', $data)) {
            $this->profile_picture = $data['profile_picture']['name'];
        }
    }

    public function store()
    {
        $path='C:\xampp\htdocs\Home_Atomic_Project\resource\Upload/';
        $uploadedFile = $path.basename($this->profile_picture);
        move_uploaded_file($_FILES['profile_picture']['tmp_name'], $uploadedFile);


        $arrData = array($this->name, $this->profile_picture);

        $sql = "Insert into profile_picture(name, profile_picture) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql); //create a object
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
