<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;
use PDO;

class Hobbies extends DB
{
    public $id = "";
    public $name = "";
    public $hobbies = "";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('hobbies', $postVariableData)) {
            $this->hobbies = $postVariableData['hobbies'];
        }


    }

    public function store()
    {
        $arrData = array($this->name, $this->hobbies);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::setMessage("Success! Data has been inserted successfully! :) ");
        else
            Message::setMessage("Failed! Data has not been inserted successfully! :( ");
        Utility::redirect('create.php');
    }

  /*  public function index()
    {
        echo "index hobbies";
    }*/


}
