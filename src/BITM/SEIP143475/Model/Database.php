<?php

    namespace App\Model;

    use PDO;
    use PDOException;


    class Database
{
    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $user="root";
    public $password="";

    public function __construct()
    {
        try {

            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
          //  echo "Connected succesfully!!";


        }
        catch(PDOException $e){
         //   echo "Connected unsuccesfully!!";

            echo $e->getMessage();
        }
    }
}
        $objDatabase= new Database();