<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class SummaryOfOrganization extends DB
{
    public $id;
    public $organization_name;
    public $organization_summary;

public function __construct(){

    parent::__construct();
}
public function index(){
//echo "I'm inside the index of SummaryOfOrganization ";
}
public function setData($data=NULL){
    if(array_key_exists('id',$data)){
    $this->id=$data['id'];
}
if(array_key_exists('organization_name',$data)){
    $this->organization_name=$data['organization_name'];
}
if(array_key_exists('organization_summary',$data)){
    $this->organization_summary=$data['organization_summary'];
}
}
public function store(){
    $arrData = array($this->organization_name,$this->organization_summary);
    $sql="Insert into summary_of_organization(organization_name, orgganization_summary) VALUES (?,?)";

    $STH= $this->DBH->prepare($sql);
    $result= $STH->execute($arrData);
if($result)
    Message::setMessage("Data has been inserted successfully!");
else
    Message::setMessage("Data has not been inserted successfully");

Utility::redirect('create.php');

}
}
