<?php

    namespace APP\BookTitle;
    use App\Message\Message;
    use App\Model\Database as DB;
    use App\Utility\Utility;
    use PDO;

class BookTitle extends DB
{
        public $id;
        public $book_title;
        public $author_name;
        public function __construct()
        {
            parent::__construct();
        }
    public function setData($postVariableData=NULL){

        if (array_key_exists('id',$postVariableData)){
            $this->id =$postVariableData['id'];
        }

        if (array_key_exists('book_title',$postVariableData)){
            $this->book_title = $postVariableData['book_title'];
        }

        if (array_key_exists('author_name',$postVariableData)){
            $this->author_name=$postVariableData['author_name'];
        }
    }
        public function index()
        {
           // echo "inside index of BookTitle";
        }
    public function store()
    {
        $arrdata= array ($this->book_title,$this->author_name);
        $sql= "Insert INTO book_title(book_title,author_name) VALUES (?,?)";
       // $sql= "Insert INTO book_title(book_title,author_name) VALUES ('$this->book_title','$this->author_name')";


        $STH=$this->DBH->prepare($sql);
        //$STH->execute();
        //$STH->execute($arrdata);
        $result = $STH->execute($arrdata);
        if($result)
            Message::setMessage("Data has been inserted successfully!! :) ");
        else
        Message::setMessage("Data has not been inserted successfully!! :( ");

        Utility::redirect('create.php');
    }
}
   // $objBooktitle=new BookTitle();
